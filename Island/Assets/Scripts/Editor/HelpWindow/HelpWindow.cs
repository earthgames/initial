﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class HelpWindow : EditorWindow
{
    [MenuItem("Tools/RPG Farming Kit Help Menu")]
    private static void OpenWindow()
    {
        Display();
    }

    public static HelpWindow Instance { private set; get; }

    private static bool projectInUploadFolder = false;

    public static void Display()
    {
        if (Instance == null)
        {
            if (EditorSceneManager.sceneCount == 0)
            {
                Instance.AddScenesToBuildList();
            }

            projectInUploadFolder = Directory.Exists(Path.Combine(Application.dataPath, "Upload"));

            var window = (HelpWindow)EditorWindow.GetWindow(typeof(HelpWindow));
            window.titleContent = new GUIContent("Help Menu");
            window.minSize = new Vector2(600, (projectInUploadFolder) ? 550 : 400);
            window.Show(true);

            Instance = window;
        }
    }

    private void OnEnable()
    {
        if (EditorPrefs.GetInt("FirstWindowLaunch", 0) == 0)
        {
            AddScenesToBuildList();
            EditorPrefs.SetInt("FirstWindowLaunch", 1);
        }
    }

    private void OnGUI()
    {
        GUILayout.BeginVertical(EditorStyles.helpBox);
        GUILayout.Label("Don't forget to implement Cinemachine and TextMeshPro from Window/Packages)", EditorStyles.boldLabel);
        GUILayout.EndVertical();

        if (projectInUploadFolder)
        {
            GUILayout.BeginVertical(EditorStyles.helpBox);
            GUILayout.Label("We have detected that the project is located in the Upload folder.\n" +
                "This project uses a custom solution for loading in game assets for saving.\n" +
                "Therefore it is advised to move content to the root folder.\n\n" +
                "Caution: if you have worked within the project already, \n" +
                "please make a backup before doing this.", EditorStyles.boldLabel);
            if (GUILayout.Button("MOVE UPLOAD CONTENT TO ROOT FOLDER (UNTESTED WITH OSX)", GUILayout.Height(35)))
            {
                MoveUploadContentToAssetRoot();
                projectInUploadFolder = false;
            }
            GUILayout.EndVertical();
        }

        GUILayout.Space(5);

        GUILayout.BeginVertical(EditorStyles.helpBox);
        GUILayout.Label("Thank you for purchasing the RPG Farming Kit!", EditorStyles.boldLabel);
        GUILayout.Label("I'm happy to announce that I will be improving this kit more, by including more features.\n" +
            "Eventually I will rebuild many things from scratch, since I don't want to risk incompatibility for users.\n" +
            "The rebuilt version will come in the form of a RPG Farming Kit V2, free for all existing users.\n" +
            "But as a seperate asset.");
        GUILayout.EndVertical();

        GUILayout.BeginVertical(EditorStyles.helpBox);

        GUILayout.Label("Helper functions", EditorStyles.boldLabel);


        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Open Start Menu", GUILayout.Height(35)))
        {
            OpenStartMenu();
        }

        if (GUILayout.Button("Open Farm Scene + Core Scene", GUILayout.Height(35)))
        {
            OpenMainAndCoreScene();
        }
        GUILayout.EndHorizontal();

        if (GUILayout.Button("Open Farm Framework Tools", GUILayout.Height(35)))
        {
            OpenKitTools();
        }

        if (GUILayout.Button("Open Project Documentation", GUILayout.Height(35)))
        {
            OpenProjectDocumentation();
        }

        if (GUILayout.Button("Rate the product (Asset Store Page)", GUILayout.Height(35)))
        {
            OpenAssetStorePage();
        }

        if (GUILayout.Button("Add all scenes to Build Settings", GUILayout.Height(35)))
        {
            AddScenesToBuildList();
        }

        GUILayout.EndVertical();

        GUILayout.BeginVertical(EditorStyles.helpBox);

        GUILayout.Label("Do you require additional information, or have questions? \nSend an email to info@low-scope.com", EditorStyles.boldLabel);

        GUILayout.EndVertical();
    }

    private void OpenAssetStorePage()
    {
        Application.OpenURL("https://assetstore.unity.com/packages/templates/packs/rpg-farming-kit-121080");
    }

    private void OpenProjectDocumentation()
    {
        Application.OpenURL("https://low-scope.com/projects/farmingkit/docs/#/");
    }

    private void OpenKitTools()
    {
        //Selection.activeObject = FarmFrameworkTools.ObtainTools();
    }

    private void OpenStartMenu()
    {
        EditorSceneManager.SaveOpenScenes();

        var getScenePath = (projectInUploadFolder) ? "Assets/Upload/Scenes" : "Assets/Scenes";

        string levelStartMenu = "";

        foreach (var item in AssetDatabase.FindAssets("t:Scene", new string[1] { getScenePath }))
        {
            var path = AssetDatabase.GUIDToAssetPath(item);

            if (Path.GetFileNameWithoutExtension(path) == "StartMenu")
                levelStartMenu = path;
        }


        if (string.IsNullOrEmpty(levelStartMenu))
        {
            Debug.LogError("Could not find scene: StartMenu");
            return;
        }

        EditorSceneManager.OpenScene(levelStartMenu);
    }

    private void OpenMainAndCoreScene()
    {
        EditorSceneManager.SaveOpenScenes();

        var getScenePath = (projectInUploadFolder) ? "Assets/Upload/Scenes" : "Assets/Scenes";

        string levelFarmPath = "";
        string levelCorePath = "";

        foreach (var item in AssetDatabase.FindAssets("t:Scene", new string[1] { getScenePath }))
        {
            var path = AssetDatabase.GUIDToAssetPath(item);

            if (Path.GetFileNameWithoutExtension(path) == "Level_Farm")
                levelFarmPath = path;
            if (Path.GetFileNameWithoutExtension(path) == "Core")
                levelCorePath = path;
        }


        if (string.IsNullOrEmpty(levelCorePath) || string.IsNullOrEmpty(levelFarmPath))
        {
            Debug.LogError("Could not find scene: Core or Level_Farm");
            return;
        }

        EditorSceneManager.OpenScene(levelFarmPath);
        EditorSceneManager.OpenScene(levelCorePath, OpenSceneMode.Additive);
    }

    private void MoveUploadContentToAssetRoot()
    {
        if (projectInUploadFolder)
        {
            AssetDatabase.StartAssetEditing();

            // Copy any leftover things that are not assets
            string uploadFolderPath = Path.Combine(Application.dataPath, "Upload");

#if UNITY_EDITOR_OSX
            uploadFolderPath = uploadFolderPath.Replace("\\", "/");
#endif

            uploadFolderPath = System.IO.Path.GetFullPath(uploadFolderPath);

            string[] filePaths = Directory.GetFiles(uploadFolderPath, "*", SearchOption.AllDirectories);
            int fileCount = filePaths.Length;

            for (int i = 0; i < fileCount; i++)
            {
                string newPath = filePaths[i].Replace(@"\Upload", "");

                if (!File.Exists(newPath))
                {
                    var dirName = Path.GetDirectoryName(newPath);
                    if (!Directory.Exists(dirName))
                        Directory.CreateDirectory(dirName);

                    File.Move(filePaths[i], newPath);
                }
            }

            Directory.Delete(uploadFolderPath, true);

            AssetDatabase.StopAssetEditing();

            AssetDatabase.Refresh(ImportAssetOptions.ForceSynchronousImport);

            AddScenesToBuildList();

        }
    }

    private void AddScenesToBuildList()
    {
        List<EditorBuildSettingsScene> addableScenes = new List<EditorBuildSettingsScene>();

        var getScenePath = (projectInUploadFolder) ? "Assets/Upload/Scenes" : "Assets/Scenes";

        foreach (var item in AssetDatabase.FindAssets("t:Scene", new string[1] { getScenePath }))
        {
            addableScenes.Add(new EditorBuildSettingsScene(new GUID(item), true));
        }

        EditorBuildSettings.scenes = addableScenes.ToArray();
    }
}
